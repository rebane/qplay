#include "libqplay.h"

static const unsigned char let2note[] = { 10, 12, 1, 3, 5, 6, 8 };

static unsigned char getnum(char *cstr, unsigned int *cstrloc){
	unsigned char t;
	t = 0;
	if(isdigit(cstr[*cstrloc + 1])){
		(*cstrloc)++;
		t = cstr[*cstrloc] - 0x30;
		if(isdigit(cstr[*cstrloc + 1])){
			(*cstrloc)++;
			t *= 10;
			t += cstr[*cstrloc] - 0x30;
			if(isdigit(cstr[*cstrloc + 1])){
				(*cstrloc)++;
				t *= 10;
				t += cstr[*cstrloc] - 0x30;
			}
		}
	}
	return(t);
}

void qplay_reset_state(struct qplay_state *qs){
	qs->tempo = 120;
	qs->len = 4;
	qs->octave = 4;
	qs->mode = 0;
}

int qplay(char *cstr, int (*mknote)(unsigned int, unsigned char, unsigned char, void *), struct qplay_state *qs, void *data){
	unsigned char t;
	unsigned int cstrloc;
	int rv;
	cstrloc = 0;
	while(cstr[cstrloc]){
		t = toupper(cstr[cstrloc]);
		if(t == 'O'){
			qs->octave = getnum(cstr, &cstrloc);
			if(qs->octave > 6)qs->octave = 6;
			//if(debug)fprintf(debug, "OCTAVE %d\n", octave);
		}else if(t == 'P'){
			t = getnum(cstr, &cstrloc);
			if(t < 1)t = 1;
			if(t > 64)t = 64;
			//if(debug)fprintf(debug, "PAUSE %d\n", t);
			rv = mknote(((240000L / qs->tempo) / t), 0, 0, data);
			if(rv)return(rv);
		}else if(t == 'N'){
			t = getnum(cstr, &cstrloc);
			if(t > 84)t = 84;
			//if(debug)fprintf(debug, "NOTE N%d\n", t);
			rv = mknote(((240000L / qs->tempo) / qs->len),
			  t, qs->mode, data);
			if(rv)return(rv);
		}else if(t == 'T'){
			qs->tempo = getnum(cstr, &cstrloc);
			if(qs->tempo < 32)qs->tempo = 32;
			//if(debug)fprintf(debug, "TEMPO %d\n", tempo);
		}else if(t == 'L'){
			qs->len = getnum(cstr, &cstrloc);
			if(qs->len < 1)qs->len = 1;
			if(qs->len > 64)qs->len = 64;
			//if(debug)fprintf(debug, "LENGTH %d\n", len);
		}else if(t > 64 && t < 72){
			//if(debug)fprintf(debug, "NOTE %c", t);
			t = (qs->octave * 12) + let2note[t - 65];
			if((cstr[cstrloc+1]=='#')||(cstr[cstrloc+1]=='+')){
				cstrloc++;
				if(t < 84){
					//if(debug)fprintf(debug, "#");
					t++;
				}
			}else if(cstr[cstrloc + 1] == '-'){
				cstrloc++;
				if(t > 1){
					//if(debug)fprintf(debug, "-");
					t--;
				}
			}
			if(cstr[cstrloc + 1] == '.'){
				//if(debug)fprintf(debug, " (3/2)\n");
				rv = mknote(((240000L / qs->tempo) / qs->len) +
				  (((240000L/qs->tempo)/qs->len) / 2),
				  t, qs->mode, data);
				if(rv)return(rv);
			}else{
				//if(debug)fprintf(debug, "\n");
				rv = mknote(((240000L / qs->tempo) / qs->len),
				  t, qs->mode, data);
				if(rv)return(rv);
			}
		}else if((t == '<') && qs->octave){
			qs->octave--;
			//if(debug)fprintf(debug, "OCTAVE-- (to %d)\n", octave);
		}else if((t == '>') && (qs->octave < 6)){
			qs->octave++;
			//if(debug)fprintf(debug, "OCTAVE++ (to %d)\n", octave);
		}else if(t == 'M' && cstr[cstrloc + 1]){
			if(toupper(cstr[cstrloc + 1]) == 'L'){
				cstrloc++;
				qs->mode = 2;
				//if(debug)fprintf(debug, "LEGATO\n");
			}else if(toupper(cstr[cstrloc + 1]) == 'N'){
				cstrloc++;
				qs->mode = 0;
				//if(debug)fprintf(debug, "NORMAL\n");
			}else if(toupper(cstr[cstrloc + 1]) == 'S'){
				cstrloc++;
				qs->mode = 1;
				//if(debug)fprintf(debug, "STACCATO\n");
			}else if(toupper(cstr[cstrloc + 1]) == 'F'){
				cstrloc++;
				rv = mknote(0, 0, 3, data);
				if(rv)return(rv);
			}else if(toupper(cstr[cstrloc + 1]) == 'B'){
				cstrloc++;
				rv = mknote(0, 0, 4, data);
				if(rv)return(rv);
			}
		}else if(t == 'X'){
			rv = mknote(0, 0, 5, data);
			if(rv)return(rv);
		}
		//if(debug)fflush(debug);
		cstrloc++;
	}
	return(0);
}

