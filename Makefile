all: console input

console:
	gcc qplay_console.c libqplay.c -o qplay_console

input:
	gcc qplay_input.c libqplay.c -o qplay_input

clean:
	rm -rf qplay_console qplay_input

