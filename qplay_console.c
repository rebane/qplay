#include <stdio.h>
#include "libqplay.h"

const unsigned int note_period[] = { 4554, 4307, 4058, 3836, 3615, 3418,
	3224, 3043, 2875, 2711, 2560, 2410 };

int mknote_console(unsigned int len, unsigned char note, unsigned char mode, void *data){
	unsigned int octave, period;
	unsigned long int t;
	int cf;
	if(mode == 3){
		printf("Foreground\n");
		return(0);
	}else if(mode == 4){
		printf("Background\n");
		return(0);
	}else if(mode == 5){
		printf("X\n");
		return(0);
	}
	if(note && (note < 85)){
		octave = ((note - 1) / 12);
		period = note_period[(note - 1) % 12];
		if(!octave)period *= 4; else
		if(octave == 1)period *= 2; else
		if(octave > 2)period /= (1 << (octave - 2));
		if(!mode){
			t = ((((len * 4) / 5) << 16) + period);
		}else if(mode == 1){
			t = (((len / 5) << 16) + period);
		}else{
			t = ((len << 16) + period);
		}
		cf = *(int *)data;
		if(ioctl(cf, KDMKTONE, t) == -1)return(-1);
	}
	usleep(len * 1000);
	return(0);
}

int qplay_console(char *cstr, struct qplay_state *state){
	int cf, t;
	cf = open("/dev/console", O_RDONLY);
	if(cf == -1)return(-1);
	t = qplay(cstr, mknote_console, state, &cf);
	close(cf);
	return(t);
}

int main(int argc, char *argv[]){
	char buffer[16384];
	struct qplay_state state;
	qplay_reset_state(&state);
	if(argc > 1){
		if(qplay_console(argv[1], &state))return(1);
	}else{
		while(1){
			fgets(&buffer[0], 16384, stdin);
			if(feof(stdin))break;
			if(qplay_console(&buffer[0], &state)){
				return(1);
			}
			printf("OK\n");
		}
	}
	return(0);
}

