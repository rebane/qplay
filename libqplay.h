#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/kd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

struct qplay_state{
	unsigned char tempo;
	unsigned char len;
	unsigned char octave;
	unsigned char mode;
};

int qplay(char *cstr, int (*mknote)(unsigned int, unsigned char, unsigned char, void *), struct qplay_state *qs, void *data);
void qplay_reset_state(struct qplay_state *qs);
	
