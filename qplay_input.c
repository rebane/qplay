#include <stdio.h>
#include <linux/input.h>
#include "libqplay.h"

const unsigned int note_freq[] = { 262, 277, 294, 311, 330, 349, 370, 392,
	415, 440, 466, 495 };

#define EVLEN (sizeof(event))
int mknote_input(unsigned int len, unsigned char note, unsigned char mode, void *data){
	struct input_event event;
	int fd, octave;
	if(mode == 3){
		printf("Foreground\n");
		return(0);
	}else if(mode == 4){
		printf("Background\n");
		return(0);
	}else if(mode == 5){
		printf("X\n");
		return(0);
	}
	if(note && (note < 85)){
		bzero(&event, sizeof(event));
		event.value = note_freq[(note - 1) % 12];
		octave = (note - 1) / 12;
		if(!octave){
			event.value /= 4;
		}else if(octave == 1){
			event.value /= 2;
		}else{
			event.value *= (1 << (octave - 2));
		}
		event.type = EV_SND;
		event.code = SND_TONE;
		fd = *(int *)data;
		if(write(fd, &event, EVLEN) != EVLEN)return(-1);
		event.value = 0;
		if(!mode){
			usleep(len * 800);
			if(write(fd, &event, EVLEN) != EVLEN)return(-1);
			usleep(len * 200);
		}else if(mode == 1){
			usleep(len * 200);
			if(write(fd, &event, EVLEN) != EVLEN)return(-1);
			usleep(len * 800);
		}else{
			usleep(len * 1000);
			if(write(fd, &event, EVLEN) != EVLEN)return(-1);
		}
	}else{
		usleep(len * 1000);
	}
	return(0);
}

int qplay_input(char *cstr, struct qplay_state *state){
	int fd, t;
	fd = open("/dev/input/pcsp", O_RDWR);
	if(fd == -1)return(-1);
	t = qplay(cstr, mknote_input, state, &fd);
	close(fd);
	return(t);
}

int main(int argc, char *argv[]){
	char buffer[16384];
	struct qplay_state state;
	qplay_reset_state(&state);
	if(argc > 1){
		if(qplay_input(argv[1], &state))return(1);
	}else{
		while(1){
			fgets(&buffer[0], 16384, stdin);
			if(feof(stdin))break;
			if(qplay_input(&buffer[0], &state)){
				return(1);
			}
			printf("OK\n");
		}
	}
	return(0);
}

